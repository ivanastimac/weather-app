import { useEffect, useState } from "react";
import axios from 'axios';
import './Weather.css';

export default function Weather() {

    const [city, setCity] = useState("Rijeka");  
    const [forecast, setForecast] = useState({temp: 0, conditionText: "", conditionImage: "", wind: 0, pressure: 0, humidity: 0, visibility: 0}); 
    const [moodCity, setMoodCity] = useState({Rijeka: "", Zagreb: "", Split: "", Rome: "", Berlin: "", Helsinki: "", Istanbul: ""}); 
    const [cityColor, setCityColor] = useState({Rijeka: "", Zagreb: "", Split: "", Rome: "", Berlin: "", Helsinki: "", Istanbul: ""}); 
    const messages = {"Great": "Party", "Not so great": "Not so party", "Okay-ish": "Party-ish", "Sad": "Suzica"};

    const changeCity = (e) => {
        setCity(e.target.value);
        getForecast();
    }

    useEffect(() => {
        getForecast();
    }, []);

    async function getForecast() {
        let promise = await axios.get("http://api.weatherapi.com/v1/current.json?key=1e91d93e064143ee95d130214210408&q=" + city + "&aqi=no");
        let data = promise.data.current;
        setForecast({temp: data.temp_c, conditionText: data.condition.text, conditionImage: data.condition.icon,
        wind: data.wind_kph, pressure:data.pressure_mb, humidity: data.humidity, visibility: data.vis_km});
        // console.log(forecast);
    }

    const changeMood = (e) => {
        const mood = e.target.value;
        setMoodCity({...moodCity, [city]: e.target.value});

        if (mood === 'Great') {
            setCityColor({...cityColor, [city]: 'pink'});
        } else if (mood === "Not so great") {
            setCityColor({...cityColor, [city]: 'blue'});
        } else if (mood === "Okay-ish") {
            setCityColor({...cityColor, [city]: 'green'});
        } else if (mood === "Sad") {
            setCityColor({...cityColor, [city]: 'gray'});
        }
        // console.log(mood);
    }

    return (
        <div class='container'>
            <div style={{backgroundColor: cityColor[city] }} >
                Choose city: <br/>
                <select class='select' onChange={changeCity}>
                    <option value="Rijeka"> Rijeka </option>
                    <option value="Zagreb"> Zagreb </option>
                    <option value="Split"> Split </option>
                    <option value="Rome"> Rome </option>
                    <option value="Berlin"> Berlin </option>
                    <option value="Helsinki"> Helsinki </option>
                    <option value="Istanbul"> Istanbul </option>
                </select>
                <div>
                    <ul>
                        <li>
                            Temperature (in centigrade): {forecast.temp}
                        </li>
                        <li>
                            Weather condition: {forecast.conditionText} <br/>
                            <img src={forecast.conditionImage} alt="Condition icon"/>
                        </li>
                        <li>
                            Wind speed (in km): {forecast.wind}
                        </li>
                        <li>
                            Pressure (in mb): {forecast.pressure}
                        </li>
                        <li>
                            Humidity: {forecast.humidity}
                        </li>
                        <li>
                            Visibility: {forecast.visibility}
                        </li>
                    </ul>
                </div>
                <div class='buttons'>               
                    <input type="button" class='button' value="Great" onClick={changeMood} />
                    <input type="button" class='button' value="Not so great" onClick={changeMood}/>
                    <input type="button" class='button' value="Okay-ish" onClick={changeMood}/>
                    <input type="button" class='button' value="Sad" onClick={changeMood}/>
                </div>
                {messages[moodCity[city]]}
            </div>
        </div>
    );
}